﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTableType {

    public string key;
    public LootTableComponent[] components;

    public LootTableType(string key, LootTableComponent[] components) {
        this.key = key;
        this.components = components;
    }
}
