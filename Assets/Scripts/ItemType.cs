﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemType {
    public const int DEFAULT_STACK_SIZE = 1;

    public string key;
    public string name;
    public int maxStackSize;

    public bool canUseInHand;

    public string iconPath;
    public Texture icon;

    public override bool Equals(object obj) {
        if (obj is ItemType) {
            ItemType it = (ItemType)obj;
            if (it.key == this.key) {
                return true;
            }
        }

        return false;
    }

    public override int GetHashCode() {
        var hashCode = -468230332;
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(key);
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
        hashCode = hashCode * -1521134295 + maxStackSize.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<Texture>.Default.GetHashCode(icon);
        return hashCode;
    }
}
