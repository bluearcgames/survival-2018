﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStack {

    public string typeKey;
    public int size = 1;

    public ItemType type;

    public ItemStack(string typeKey) {
        this.typeKey = typeKey;
        this.size = 1;
        type = ItemManager.BY_KEY.get(typeKey);
    }

    public ItemStack(ItemType type) {
        this.typeKey = type.key;
        this.type = type;
        this.size = 1;
    }

    public ItemStack(string typeKey, int size) {
        this.typeKey = typeKey;
        this.size = size;
        type = ItemManager.BY_KEY.get(typeKey);
    }

    public ItemStack(ItemType type, int size) {
        this.typeKey = type.key;
        this.type = type;
        this.size = size;
    }

    public ItemStack Split() {
        return Split(this.size / 2);
    }

    public ItemStack Split(int amount) {
        if (this.size < amount) {
            amount = this.size;
        }
        
        this.size -= amount;
        return new ItemStack(this.type, amount);
    }

    public ItemStack Clone() {
        return new ItemStack(type, size);
    }

    public ItemStack Add(ItemStack stack) {
        if (stack.size + this.size <= type.maxStackSize) {
            this.size += stack.size;
            stack.size = 0;
        } else if (this.size < type.maxStackSize) {
            int diff = type.maxStackSize - this.size;
            this.size += diff;
            stack.size -= diff;
        }

        return stack;
    }

    public ItemStack Remove(int amount) {
        if (amount >= size) {
            size = 0;
            return new ItemStack(type, size);
        }

        size = size - amount;
        return new ItemStack(type, amount);
    }
}
