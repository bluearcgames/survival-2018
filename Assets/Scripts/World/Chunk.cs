﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chunk : Tickable {

    protected Block[,,] blocks;

    protected int x;
    protected int y;
    protected int z;

    protected MeshData meshData;

    protected World world;
    protected GameObject go;

    protected bool hasGenerated = false;
    protected bool hasDrawn = false;
    protected bool drawnLock = false;
    protected bool hasRendered = false;
    protected bool renderLock = false;
    protected bool dirty = true;

    public Chunk(ChunkData data) {
        this.x = data.x;
        this.y = data.y;
        this.z = data.z;

        blocks = new Block[getChunkSizeX(), getChunkSizeY(), getChunkSizeZ()];
        for (int bx = 0; bx < getChunkSizeX(); bx++) {
            for (int by = 0; by < getChunkSizeY(); by++) {
                for (int bz = 0; bz < getChunkSizeZ(); bz++) {
                    blocks[x, y, z] = new Block(this, data.blocks[bx, by, bz], bx, by, bz);
                }
            }
        }

        hasGenerated = true;
    }

    public Chunk(World world, int x, int y, int z) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;

        blocks = new Block[getChunkSizeX(), getChunkSizeY(), getChunkSizeZ()];
        for (int bx = 0; bx < getChunkSizeX(); bx++) {
            for (int by = 0; by < getChunkSizeY(); by++) {
                for (int bz = 0; bz < getChunkSizeZ(); bz++) {
                    BlockType type = BlockManager.BY_KEY.get("AIR");

                    if (by <= 5) {
                        type = BlockManager.BY_KEY.get("STONE");
                    }

                    blocks[bx, by, bz] = new Block(this, type, bx, by, bz);
                }
            }
        }

        hasGenerated = true;
    }

    // Use this for initialization
    public void start() {

    }

    public void tick() {

    }

    public void update() {
        if (dirty) {
            if (!drawnLock && !renderLock) {
                hasDrawn = false;
                hasRendered = false;
                dirty = false;
            }
        }

        if (!hasDrawn && hasGenerated && !drawnLock) {
            drawnLock = true;

            meshData = new MeshData();

            for (int bx = 0; bx < getChunkSizeX(); bx++) {
                for (int by = 0; by < getChunkSizeY(); by++) {
                    for (int bz = 0; bz < getChunkSizeZ(); bz++) {
                        meshData.merge(blocks[bx, by, bz].draw());
                    }
                }
            }

            hasDrawn = true;
            drawnLock = false;
        }
    }

    public virtual void onUnityUpdate() {
        if (hasGenerated && !hasRendered && hasDrawn) {
            renderLock = true;

            Mesh mesh = meshData.toMesh();

            if (go == null) {
                go = new GameObject();
            }

            Transform t = go.transform;

            if (t.transform.GetComponent<MeshFilter>() == null) {
                t.gameObject.AddComponent<ChunkScript>();
                t.gameObject.GetComponent<ChunkScript>().chunk = this;

                t.gameObject.AddComponent<MeshFilter>();
                t.gameObject.AddComponent<MeshRenderer>();
                t.gameObject.AddComponent<MeshCollider>();
                t.gameObject.GetComponent<MeshRenderer>().material = TextureAtlas.atlasMaterial;
                t.transform.position = new Vector3(x * getChunkSizeX(), y * getChunkSizeY(), z * getChunkSizeZ());
            }

            t.transform.GetComponent<MeshFilter>().sharedMesh = mesh;
            t.transform.GetComponent<MeshCollider>().sharedMesh = mesh;

            hasRendered = true;
            renderLock = false;
        }
    }

    public Block getBlock(Vector3 loc) {
        return getBlock((int)loc.x, (int)loc.y, (int)loc.z);
    }

    public Block getBlock(int bx, int by, int bz) {
        return getBlockLocal(bx - (x * getChunkSizeX()), by - (y * getChunkSizeY()), bz - (z * getChunkSizeZ()));
    }

    public Block getBlockLocal(Vector3 loc) {
        return getBlockLocal((int)loc.x, (int)loc.y, (int)loc.z);
    }

    public Block getBlockLocal(int bx, int by, int bz) {
        if (blocks.GetLength(0) <= bx) {
            Chunk chunk = getNeightbor(Direction.EAST);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx - getChunkSizeX(), by, bz);
        }

        if (bx < 0) {
            Chunk chunk = getNeightbor(Direction.WEST);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx + getChunkSizeX(), by, bz);
        }

        if (blocks.GetLength(1) <= by) {
            Chunk chunk = getNeightbor(Direction.UP);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx, by - getChunkSizeY(), bz);
        }

        if (by < 0) {
            Chunk chunk = getNeightbor(Direction.DOWN);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx, by + getChunkSizeY(), bz);
        }

        if (blocks.GetLength(2) <= bz) {
            Chunk chunk = getNeightbor(Direction.NORTH);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx, by, bz - getChunkSizeZ());
        }

        if (bz < 0) {
            Chunk chunk = getNeightbor(Direction.SOUTH);
            if (chunk == null) {
                return null;
            }

            return chunk.getBlockLocal(bx, by, bz + getChunkSizeZ());
        }

        return blocks[bx, by, bz];
    }

    public Chunk getNeightbor(Direction d) {
        switch (d) {
            case Direction.NORTH: {
                    return world.getChunk(x, y, z + 1);
                }
            case Direction.SOUTH: {
                    return world.getChunk(x, y, z - 1);
                }
            case Direction.EAST: {
                    return world.getChunk(x + 1, y, z);
                }
            case Direction.WEST: {
                    return world.getChunk(x - 1, y, z);
                }
            case Direction.UP: {
                    return world.getChunk(x, y + 1, z);
                }
            case Direction.DOWN: {
                    return world.getChunk(x, y - 1, z);
                }
        }

        return null;
    }

    public void markDirty() {
        dirty = true;
    }

    public void setBlock(BlockType type, Vector3 loc) {
        setBlock(type, (int)loc.x, (int)loc.y, (int)loc.z);
    }

    public void setBlock(BlockType type, int bx, int by, int bz) {
        Debug.Log("Setting [" + bx + ", " + by + ", " + bz + "] to type " + type.name + "!");
        Debug.Log("Chunk [" + x + ", " + y + ", " + z + "]!");
        Debug.Log("World Size [" + getChunkSizeX() + ", " + getChunkSizeY() + ", " + getChunkSizeZ() + "]!");
        setBlockLocal(type, bx - (x * getChunkSizeX()), by - (y * getChunkSizeY()), bz - (z * getChunkSizeZ()));
    }

    public void setBlockLocal(BlockType type, Vector3 loc) {
        setBlockLocal(type, (int)loc.x, (int)loc.y, (int)loc.z);
    }

    public void setBlockLocal(BlockType type, int bx, int by, int bz) {
        if (type == null) {
            return;
        }

        Debug.Log("Setting [" + bx + ", " + by + ", " + bz + "] to type " + type.name + "!");

        blocks[bx, by, bz] = new Block(this, type, bx, by, bz);

        if (bx == 0) {
            Chunk nc = getNeightbor(Direction.WEST);
            if (nc != null) {
                nc.markDirty();
            }
        } else if (bx + 1 == getChunkSizeX()) {
            Chunk nc = getNeightbor(Direction.EAST);
            if (nc != null) {
                nc.markDirty();
            }
        }

        if (by == 0) {
            Chunk nc = getNeightbor(Direction.DOWN);
            if (nc != null) {
                nc.markDirty();
            }
        } else if (by + 1 == getChunkSizeY()) {
            Chunk nc = getNeightbor(Direction.UP);
            if (nc != null) {
                nc.markDirty();
            }
        }

        if (bz == 0) {
            Chunk nc = getNeightbor(Direction.SOUTH);
            if (nc != null) {
                nc.markDirty();
            }
        } else if (bz + 1 == getChunkSizeZ()) {
            Chunk nc = getNeightbor(Direction.NORTH);
            if (nc != null) {
                nc.markDirty();
            }
        }

        dirty = true;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public short getChunkSizeX() {
        return world.getChunkSizeX();
    }

    public short getChunkSizeY() {
        return world.getChunkSizeY();
    }

    public short getChunkSizeZ() {
        return world.getChunkSizeZ();
    }
}
