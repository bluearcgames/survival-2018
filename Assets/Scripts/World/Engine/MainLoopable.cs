﻿using System;
using System.Collections.Generic;

[Serializable]
public class MainLoopable : Loopable {
    private static MainLoopable instance;

    private Logger logger;
    private List<Loopable> registeredLoopable = new List<Loopable>();

    public World world = new World();

    public static void instantiate() {
        instance = new MainLoopable();
        instance.logger = Logger.mainLog;
    }

    public static MainLoopable getInstance() {
        return instance;
    }

    public MainLoopable() {
        registerLoopable(world);
    }

    public void registerLoopable(Loopable l) {
        registeredLoopable.Add(l);
    }

    public void removeLoopable(Loopable l) {
        registeredLoopable.Remove(l);
    }

    public void start() {
        foreach (Loopable l in registeredLoopable) {
            l.start();
        }

        logger.start();

        Logger.log("Main loopable started.");
        UnityEngine.Debug.Log("Main loopable started.");
    }

    public void update() {
        foreach (Loopable l in registeredLoopable) {
            l.update();
        }

        logger.update();
    }

    public void onApplicationQuit() {
        foreach (Loopable l in registeredLoopable) {
            l.onApplicationQuit();
        }

        logger.onApplicationQuit();
    }
}