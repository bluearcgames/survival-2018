﻿using System.Collections.Generic;

public class Logger : Loopable {
    public static Logger mainLog = new Logger();

    private List<string> mainLogTxt = new List<string>();

    public static void log(string log) {
        //mainLog.mainLogTxt.Add(log);
        UnityEngine.Debug.Log(log);
    }

    public static void log(System.Exception e) {
        //mainLog.mainLogTxt.Add(e.Message);
        //mainLog.mainLogTxt.Add(e.StackTrace.ToString());
        UnityEngine.Debug.Log(e.Message);
        UnityEngine.Debug.Log(e.StackTrace.ToString());
    }

    public void start() {
    }

    public void update() {
        //List<string> newList = new List<string>(mainLogTxt);
        //System.IO.File.WriteAllLines("mainLog.txt", newList.ToArray());
    }

    public void onApplicationQuit() {
    }
}
