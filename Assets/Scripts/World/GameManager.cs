﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class GameManager : MonoBehaviour {

    private const string WORLD_DATA = @"Assets\Resources\World\world_data.json";

    public float saveInterval = 5;
    private float interval = 0;

    public World world;

    private MainLoopable main;

    public void Start() {
        // Load World Types
        if (File.Exists(WORLD_DATA)) {
            //world = JsonConvert.DeserializeObject<World>(File.ReadAllText(WORLD_DATA));
        }

        TextureAtlas.instance.CreateAtlas();

        MainLoopable.instantiate();

        main = MainLoopable.getInstance();
        main.start();
        world = main.world;

        Logger.log("Main Started");
    }

    public void Update() {
        main.update();

        /*if (interval > saveInterval) {
            Debug.Log("Saving...");
            string json = JsonConvert.SerializeObject(world);
            Debug.Log(json);
            File.WriteAllText(WORLD_DATA, json);
            interval = 0;
        }
        interval += UnityEngine.Time.deltaTime;*/
    }

    public void OnApplicationQuit() {
        main.onApplicationQuit();
    }
}
