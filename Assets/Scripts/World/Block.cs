﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Block : Tickable {

    protected int x;
    protected int y;
    protected int z;

    // Texturing
    protected UvMap uvMapTop;
    protected UvMap uvMap;

    protected BlockType type;
    protected Chunk chunk;

    // Marching Cubes
    protected float isoLevel;
    protected Vector3[] grid = new Vector3[8];
    
    public Block(Chunk chunk, BlockData data, int x, int y, int z) {
        this.chunk = chunk;
        this.type = BlockManager.BY_KEY.get(data.typeKey);
        this.x = x;
        this.y = y;
        this.z = z;

        Initialize();
    }

    public Block(Chunk chunk, BlockType type, int x, int y, int z) {
        this.chunk = chunk;
        this.type = type;
        this.x = x;
        this.y = y;
        this.z = z;

        Initialize();
    }

    public void Initialize() {
        if (type != null) {
            String textureKey = type.key.ToLower().Replace(" ", "-");
            UvMap map = UvMap.getUvMap(textureKey);
            if (map != null) {
                uvMap = map;
            }

            uvMapTop = UvMap.getUvMap(textureKey + "-top");
        }
    }

    public Block getNeighborByFace(BlockFace face) {
        switch (face) {
            case BlockFace.UP: {
                    return getNeightbor(Direction.UP);
                }
            case BlockFace.DOWN: {
                    return getNeightbor(Direction.DOWN);
                }
            case BlockFace.NORTH: {
                    return getNeightbor(Direction.NORTH);
                }
            case BlockFace.SOUTH: {
                    return getNeightbor(Direction.SOUTH);
                }
            case BlockFace.EAST: {
                    return getNeightbor(Direction.EAST);
                }
            case BlockFace.WEST: {
                    return getNeightbor(Direction.WEST);
                }
        }

        return null;
    }

    public Block getNeightbor(Direction d) {
        switch (d) {
            case Direction.UP: {
                    return chunk.getBlockLocal(x, y + 1, z);
                }
            case Direction.DOWN: {
                    return chunk.getBlockLocal(x, y - 1, z);
                }
            case Direction.NORTH: {
                    return chunk.getBlockLocal(x, y, z + 1);
                }
            case Direction.SOUTH: {
                    return chunk.getBlockLocal(x, y, z - 1);
                }
            case Direction.EAST: {
                    return chunk.getBlockLocal(x + 1, y, z);
                }
            case Direction.WEST: {
                    return chunk.getBlockLocal(x - 1, y, z);
                }
        }

        return null;
    }

    public void tick() {

    }

    public void start() {
        isoLevel = type.isTransparent ? 0 : 1;
    }

    public void update() {

    }

    public void onUnityUpdate() {

    }

    public MeshData draw() {
        return BlockRenderer.drawBlock(this);
    }

    public List<ItemStack> getItemStack() {
        if (type.lootTable != null) {
            return type.lootTable.GenerateLoot();
        }

        return new List<ItemStack>();
    }

    public UvMap getUvMapTop() {
        return uvMapTop != null ? uvMapTop : uvMap;
    }

    public UvMap getUvMap() {
        return uvMap;
    }

    public BlockType getType() {
        return type;
    }

    public Chunk getChunk() {
        return chunk;
    }

    // Marching Cubes
    public float getIsoLevel() {
        return isoLevel;
    }

    public Vector3[] getGrid() {
        return grid;
    }

    public Vector3 getLocation() {
        return new Vector3(x + (chunk.getX() * chunk.getChunkSizeX()), y + (chunk.getY() * chunk.getChunkSizeY()), z + (chunk.getZ() * chunk.getChunkSizeZ()));
    }

    public Vector3 getLocalLocation() {
        return new Vector3(x, y, z);
    }
}
