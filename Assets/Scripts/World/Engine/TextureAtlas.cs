﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class TextureAtlas {
    private const string BLOCKS_FOLDER = @"Assets\Resources\Blocks\";

    public static readonly TextureAtlas instance = new TextureAtlas();

    public static Material atlasMaterial { get; private set; }
    public static int PIXEL_WIDTH = 256;
    public static int PIXEL_HEIGHT = 256;
    public static int ATLAS_WIDTH = 8;
    public static int ATLAS_HEIGHT = 8;

    public void CreateAtlas() {

        Shader shader = Shader.Find("Standard");
        atlasMaterial = new Material(shader);

        string ext = ".png";

        Logger.log("Exists? " + Directory.Exists(BLOCKS_FOLDER));
        string[] images = Directory.GetFiles(BLOCKS_FOLDER, "*" + ext);
        Logger.log(images.Length + " images found.");
        for (int i = 0; i < images.Length; i++) {
            Logger.log("Image found for atlas: " + images[i]);
        }

        ATLAS_WIDTH = Mathf.CeilToInt(Mathf.Sqrt(images.Length)) * PIXEL_WIDTH;
        ATLAS_HEIGHT = Mathf.CeilToInt(Mathf.Sqrt(images.Length)) * PIXEL_HEIGHT;


        Texture2D atlas = new Texture2D(ATLAS_WIDTH, ATLAS_HEIGHT);
        Texture2D atlas_alpha = new Texture2D(ATLAS_WIDTH, ATLAS_HEIGHT);
        Texture2D atlas_normal = new Texture2D(ATLAS_WIDTH, ATLAS_HEIGHT);

        int count = 0;
        for (int x = 0; x < ATLAS_WIDTH / PIXEL_WIDTH; x++) {
            for (int y = 0; y < ATLAS_HEIGHT / PIXEL_HEIGHT; y++) {
                if (count > images.Length - 1) {
                    goto end;
                }
                Texture2D temp = new Texture2D(0, 0);
                temp.LoadImage(File.ReadAllBytes(images[count]));
                atlas.SetPixels(x * PIXEL_WIDTH, y * PIXEL_HEIGHT, PIXEL_WIDTH, PIXEL_HEIGHT, temp.GetPixels());


                float startX = x * PIXEL_WIDTH;
                float startY = y * PIXEL_HEIGHT;
                float perPixelRatioX = 1.0f / (float)atlas.width;
                float perPixelRatioY = 1.0f / (float)atlas.height;
                
                startX *= perPixelRatioX;
                startY *= perPixelRatioY;

                float textXSize = perPixelRatioX * PIXEL_WIDTH;
                float textYSize = perPixelRatioY * PIXEL_HEIGHT;

                float endX = startX + textXSize;
                float endY = startY + textYSize;
                
                string imageName = images[count];
                imageName = imageName.Replace(BLOCKS_FOLDER, "");
                imageName = imageName.Replace(ext, "");
                
                UvMap m = new UvMap(imageName, new List<Vector2> {
                    new Vector2(startX, startY),
                    new Vector2(endX, startY),
                    new Vector2(startX, endY),
                    new Vector2(endX, endY)
                }, startX, startY, textXSize, textYSize);                
                
                m.register();

                count++;
            }
        }

    end:;
        atlas.filterMode = FilterMode.Point;
        atlas.Apply();
        atlasMaterial.mainTexture = atlas;
        //File.WriteAllBytes("Assets//atlas.png", atlas.EncodeToPNG());
        //atlasMaterial = Resources.Load<Material>("Materials/Atlas");
    }
}
