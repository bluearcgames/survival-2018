﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CraftingSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public bool hovered;

    public CraftingRecipe recipe;

    public GameObject icon;
    public GameObject text;

    private InventoryUI inventoryUI;

    // Use this for initialization
    public CraftingSlot() {
        hovered = false;
    }

    public void Start() {
        inventoryUI = GameObject.FindWithTag("Player").GetComponent<InventoryUI>();
    }

    // Update is called once per frame
    public void Update() {
        if (recipe != null && recipe.getType() != null && recipe.getType().icon != null) {
            icon.GetComponent<RawImage>().texture = recipe.getType().icon;
            icon.GetComponent<RawImage>().enabled = true;

            text.GetComponent<Text>().text = recipe.getType().name;
        } else {
            icon.GetComponent<RawImage>().texture = null;
            icon.GetComponent<RawImage>().enabled = false;

            text.GetComponent<Text>().text = "";
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        hovered = true;
    }

    public void OnPointerExit(PointerEventData eventData) {
        hovered = false;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (recipe != null && recipe.getType() != null) {
            foreach (KeyValuePair<string, int> ingredient in recipe.ingredients) {
                if (!inventoryUI.inventory.HasAmountByTypeKey(ingredient.Key, ingredient.Value)) {
                    Debug.Log("Not enough " + ingredient.Key);
                    return;
                }
            }

            foreach (KeyValuePair<string, int> ingredient in recipe.ingredients) {
                inventoryUI.inventory.RemoveByTypeKey(ingredient.Key, ingredient.Value);
            }

            inventoryUI.inventory.AddItemStack(recipe.getType(), 1);
        }
    }
}
