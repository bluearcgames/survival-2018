﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

/**
 * Wrapper class for dictionary to allow it to function listringe a Java HashMap
 */
[Serializable]
public class HashMap<K, V> : Dictionary<K, V> {
    // Constructs an empty HashMap.
    public HashMap() {
    }

    // Constructs a new HashMap with the same mappings as the specified Map.
    public HashMap(HashMap<K, V> map) {
        putAll(map);
    }

    // Associates the specified value with the specified key in this map.
    public void put(K key, V value) {
        this[key] = value;
    }

    // Copies all of the mappings from the specified map to this map.
    public void putAll(HashMap<K, V> map) {
        foreach (KeyValuePair<K, V> entry in map) {
            put(entry.Key, entry.Value);
        }
    }

    // If the specified key is not already associated with a value associates it with the given value and returns true, else returns false.
    public bool putIfAbsent(K key, V value) {
        if (containsKey(key)) {
            return false;
        }

        put(key, value);
        return true;
    }

    // Returns the value to which the specified key is mapped, or the default value for the type if this map contains no mapping for the key.
    public V get(K key) {
        if (containsKey(key)) {
            return this[key];
        }

        return default(V);
    }

    // Returns the value to which the specified key is mapped, or defaultValue if this map contains no mapping for the key.
    public V getOrDefault(K key, V defaultValue) {
        if (containsKey(key)) {
            return this[key];
        }
        return defaultValue;
    }

    // Removes the mapping for the specified key from this map if present.
    public void remove(K key) {
        if (containsKey(key)) {
            this.Remove(key);
        }
    }

    // Returns the number of key-value mappings in this map.
    public int size() {
        return this.Count;
    }

    // Returns a List view of the keys contained in this map.
    public List<K> keys() {
        return this.Keys.ToList();
    }

    // Returns a List view of the values contained in this map.
    public List<V> values() {
        return this.Values.ToList();
    }

    // Returns true if this map contains a mapping for the specified key.
    public bool containsKey(K key) {
        return this.ContainsKey(key);
    }

    // Returns true if this map maps one or more keys to the specified value.
    public bool containsValue(V value) {
        return this.ContainsValue(value);
    }

    // Returns a shallow copy of this HashMap instance: the keys and values themselves are not cloned.
    public HashMap<K, V> clone() {
        return new HashMap<K, V>(this);
    }

    // Removes all of the mappings from this map.
    public void clear() {
        this.Clear();
    }

    // Returns true if this map contains no key-value mappings.
    public bool isEmpty() {
        return size() <= 0;
    }
}
