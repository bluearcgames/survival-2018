﻿public interface Tickable {
    void tick();
    void start();
    void update();
    void onUnityUpdate();
}