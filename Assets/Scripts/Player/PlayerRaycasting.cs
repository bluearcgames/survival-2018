﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRaycasting : MonoBehaviour {

    public GameObject playerObj;
    public GameObject tools;

    private Player player;
    private InventoryUI inventoryUI;
    private ItemInHandController iihController;

    // Use this for initialization
    void Start () {
        player = playerObj.GetComponent<Player>();
        inventoryUI = playerObj.GetComponent<InventoryUI>();
        iihController = tools.GetComponent<ItemInHandController>();
    }
	
	// Update is called once per frame
	void Update () {

        RaycastHit hit = new RaycastHit();

        Physics.SphereCast(transform.position, 0.25f, transform.forward * 8f, out hit);
        float interactableDistance = 6f;

        Debug.DrawRay(transform.position, transform.forward * 8f, Color.red);

        if (hit.collider != null) {
            if (Input.GetKeyDown(KeyCode.E)) {
                if (Vector3.Distance(hit.collider.gameObject.transform.position, transform.position) <= interactableDistance) {
                    switch (hit.collider.gameObject.tag) {
                        case "Item":
                            if (hit.collider.gameObject.GetComponent<Item>().type.canUseInHand) {
                                ItemStack newStack = new ItemStack(hit.collider.gameObject.GetComponent<Item>().type);
                                ItemStack leftOverStack = inventoryUI.inventory.AddItemStack(newStack);
                                if (leftOverStack == null) {
                                    if (player.itemInHand == null) {
                                        iihController.SetActive(newStack);
                                    }
                                }
                                Destroy(hit.collider.gameObject);
                            }
                            break;
                    }
                }
            }
        }

    }
}
