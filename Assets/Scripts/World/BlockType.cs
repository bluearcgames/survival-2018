﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BlockType {

    public readonly string key;
    public readonly string name;
    public readonly bool isTransparent;
    public readonly bool isStatic;
    public readonly LootTable lootTable;

    public readonly HashMap<Direction, BlockShape> mesh;

    public BlockType(string key, string name, bool isTransparent, bool isStatic, LootTable lootTable, HashMap<Direction, BlockTypeMeshData> mesh) {
        this.key = key;
        this.name = name;
        this.isTransparent = isTransparent;
        this.isStatic = isStatic;
        this.lootTable = lootTable;
        this.mesh = new HashMap<Direction, BlockShape>();
        if (mesh != null) {
            foreach (Direction d in mesh.keys()) {
                BlockTypeMeshData md = mesh.get(d);
                if (md != null) {
                    this.mesh.put(d, md.getShape());
                }
            }
        }
    }

    public override int GetHashCode() {
        return key.GetHashCode();
    }

    public override bool Equals(System.Object obj) {
        if (obj == null || obj.GetType() != typeof(BlockType)) {
            return false;
        }

        BlockType type = (BlockType)obj;

        return type.key == this.key;
    }
}
