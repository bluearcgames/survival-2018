﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTable : MonoBehaviour {

    private static readonly System.Random random = new System.Random();

    public string typeKey;
    public LootTableType type;

	// Use this for initialization
	void Start () {
        type = LootTableManager.BY_KEY.get(typeKey);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public List<ItemStack> GenerateLoot() {
        List<ItemStack> loot = new List<ItemStack>();
        for (int i = 0; i < type.components.Length; i++) {
            LootTableComponent comp = type.components[i];

            ItemStack item = GenerateComponentLoot(comp);
            if (item != null) {
                loot.Add(item);
            }
        }
        return loot;
    }

    public ItemStack GenerateComponentLoot(LootTableComponent comp) {
        lock (random) { // synchronize
            double p = random.NextDouble();
            int amount = random.Next(comp.min, comp.max + 1);

            if (p <= comp.probability) {
                return new ItemStack(comp.getType(), amount);
            }
        }

        return null;
    }
}
