﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockRenderer {

    public static MeshData drawBlock(Block block) {
        if (block.getType() == null) {
            return new MeshData();
        }

        return block.getType().isStatic ? drawStatic(block) : drawNonStatic(block);
    }

    /**
     * Marching Cubes Rendering
     */
    private static MeshData drawNonStatic(Block block) {
        MeshData d = new MeshData();

        List<Triangle> triangles = new List<Triangle>();
        int numTriangles = Poligonizator.polygonise(block, ref triangles, 0.5f);
        if (numTriangles == 0) {
            return d;
        }

        UvMap uvMap = block.getUvMap();
        if (block.getType().isTransparent) {
            Block b = block.getNeightbor(Direction.DOWN);
            if (b != null) {
                uvMap = b.getUvMap();
            }
        }

        if (uvMap == null) {
            return d;
        }

        List<Vector3> vertList = new List<Vector3>();
        List<int> triangleList = new List<int>();
        List<Vector2> uvList = new List<Vector2>();

        int count = 0;
        foreach (Triangle triangle in triangles) {
            for (int i = 0; i < 3; i++) {
                Vector3 vertex = triangle.p[i];
                vertList.Add(vertex);
                triangleList.Add(count);
                count++;

                /*float startX = (vertex.x % 1 * TextureAtlas.PIXEL_WIDTH) + (uvMap.x * TextureAtlas.PIXEL_WIDTH);
                float startY = (vertex.z % 1 * TextureAtlas.PIXEL_HEIGHT) + (uvMap.y * TextureAtlas.PIXEL_HEIGHT);
                float perPixelRatioX = 1.0f / (float)TextureAtlas.ATLAS_WIDTH;
                float perPixelRatioY = 1.0f / (float)TextureAtlas.ATLAS_HEIGHT;

                startX *= perPixelRatioX;
                startY *= perPixelRatioY;*/

                float startX = vertex.x % 1;
                float startY = vertex.z % 1;

                uvList.Add(new Vector2(startX, startY));
            }
        }

        d.merge(new MeshData(
          vertList,
          triangleList,
          uvList
        ));

        return d;
    }

    /**
     * Cube Block Rendering
     */
    private static List<int> clockwiseTri = new List<int>() { 0, 1, 2, 3, 2, 1 };
    private static List<int> counterClockwiseTri = new List<int>() { 0, 2, 1, 3, 1, 2 };

    private static MeshData drawStatic(Block block) {
        MeshData d = new MeshData();

        if (block.getType().isTransparent) {
            return d;
        }

        if (block.getUvMap() == null) {
            return d;
        }

        List<Vector2> uvs = block.getUvMap().uvMap;
        List<Vector2> uvsTop = block.getUvMapTop().uvMap;

        Block neightbor = block.getNeightbor(Direction.DOWN);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.DOWN)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.DOWN), block.getUvMap()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                    new Vector3(0, 0, 0),
                    new Vector3(0, 0, 1),
                    new Vector3(1, 0, 0),
                    new Vector3(1, 0, 1)
                    },
                    counterClockwiseTri,
                    uvs
                ));
            }
        }
        
        neightbor = block.getNeightbor(Direction.UP);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.UP)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.UP), block.getUvMapTop()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                        new Vector3(0, 1, 0),
                        new Vector3(0, 1, 1),
                        new Vector3(1, 1, 0),
                        new Vector3(1, 1, 1)
                    },
                    clockwiseTri,
                    uvsTop
                ));
            }
        }
        
        neightbor = block.getNeightbor(Direction.EAST);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.EAST)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.EAST), block.getUvMap()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                        new Vector3(1, 0, 0),
                        new Vector3(1, 0, 1),
                        new Vector3(1, 1, 0),
                        new Vector3(1, 1, 1)
                    },
                    counterClockwiseTri,
                    uvs
                ));
            }
        }
        
        neightbor = block.getNeightbor(Direction.WEST);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.WEST)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.WEST), block.getUvMap()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                        new Vector3(0, 0, 0),
                        new Vector3(0, 0, 1),
                        new Vector3(0, 1, 0),
                        new Vector3(0, 1, 1)
                    },
                    clockwiseTri,
                    uvs
                ));
            }
        }
        
        neightbor = block.getNeightbor(Direction.NORTH);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.NORTH)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.NORTH), block.getUvMap()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                        new Vector3(0, 0, 1),
                        new Vector3(1, 0, 1),
                        new Vector3(0, 1, 1),
                        new Vector3(1, 1, 1)
                    },
                    clockwiseTri,
                    uvs
                ));
            }
        }
        
        neightbor = block.getNeightbor(Direction.SOUTH);
        if (neightbor == null || neightbor.getType().isTransparent) {
            if (block.getType().mesh.containsKey(Direction.SOUTH)) { // has shape
                d.merge(buildFromShape(block.getType().mesh.get(Direction.SOUTH), block.getUvMap()));
            } else { // else full side
                d.merge(new MeshData(
                    new List<Vector3>() {
                        new Vector3(0, 0, 0),
                        new Vector3(1, 0, 0),
                        new Vector3(0, 1, 0),
                        new Vector3(1, 1, 0)
                    },
                    counterClockwiseTri,
                    uvs
                ));
            }
        }
        
        d.addPos(block.getLocalLocation());

        return d;
    }

    private static MeshData buildFromShape(BlockShape shape, UvMap uvMap) {
        return new MeshData(
            shape.verts,
            shape.tris,
            calcUvs(shape, uvMap)
        );
    }

    private static List<Vector2> calcUvs(BlockShape shape, UvMap uvMap) {
        List<Vector2> uvs = new List<Vector2>();

        foreach (Vector2 uv in shape.uvs) {
            uvs.Add(new Vector2((uv.x * uvMap.textXSize) + uvMap.startX, (uv.y * uvMap.textYSize) + uvMap.startY));
        }

        return uvs;
    }
}
