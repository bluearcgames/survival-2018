﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class CraftingManager : MonoBehaviour {

    private const string CRAFTING_RECIPES_DATA = @"Assets\Resources\conf\crafting_recipes.json";

    public static List<CraftingRecipe> ALL;
    public static HashMap<string, CraftingRecipe> BY_KEY = new HashMap<string, CraftingRecipe>();
    public static HashMap<string, List<CraftingRecipe>> BY_TYPE_KEY = new HashMap<string, List<CraftingRecipe>>();
    public static HashMap<string, List<CraftingRecipe>> BY_CATEGORY = new HashMap<string, List<CraftingRecipe>>();

    // Use this for initialization
    void Awake() {
        // Load Crafting Recipes
        if (File.Exists(CRAFTING_RECIPES_DATA)) {
            ALL = JsonConvert.DeserializeObject<List<CraftingRecipe>>(File.ReadAllText(CRAFTING_RECIPES_DATA));
            Debug.Log(JsonConvert.SerializeObject(ALL));
        }

        foreach (CraftingRecipe recipe in ALL) {
            BY_KEY[recipe.recipeKey] = recipe;

            if (!BY_TYPE_KEY.containsKey(recipe.typeKey)) {
                BY_TYPE_KEY[recipe.typeKey] = new List<CraftingRecipe>();
            }
            BY_TYPE_KEY[recipe.typeKey].Add(recipe);

            foreach (string category in recipe.categories) {
                if (!BY_CATEGORY.containsKey(category)) {
                    BY_CATEGORY[category] = new List<CraftingRecipe>();
                }
                BY_CATEGORY[category].Add(recipe);
            }
        }
    }
}
