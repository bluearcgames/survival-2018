﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerStats {

    public HashMap<string, float> attributes;
    public HashMap<string, Skill> skills;
    
    private HashMap<string, List<Skill>> skillsByAttribute;

    // Use this for initialization
    void Start () {
        foreach (Skill s in skills.values()) {
            foreach (string a in s.attributeEffectsPerLevel.keys()) {
                if (!skillsByAttribute.containsKey(a)) {
                    skillsByAttribute.put(a, new List<Skill>());
                }

                skillsByAttribute.get(a).Add(s);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public float getAttributeValue(string name) {
        float a = attributes.get(name);
        if (a <= 0) {
            return a;
        }

        float finalA = a;
        List<Skill> attributeSkills = skillsByAttribute.get(name);
        if (attributeSkills != null) {
            foreach (Skill s in attributeSkills) {
                finalA += a * s.attributeEffectsPerLevel.get(name);
            }
        }

        return finalA;
    }

    public Skill getSkill(string name) {
        return skills.get(name);
    }
}
