﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshData {
    private List<Vector3> verts = new List<Vector3>();
    private List<int> tris = new List<int>();
    private List<Vector2> uvs = new List<Vector2>();

    public MeshData(List<Vector3> v, List<int> i, List<Vector2> u) {
        this.verts = v;
        this.tris = i;
        this.uvs = u;
    }

    public MeshData() {
    }

    public void addPos(Vector3 loc) {
        for (int i = 0; i < verts.Count; i++) {
            verts[i] = verts[i] + loc;
        }
    }

    public void merge(MeshData m) {
        if (m.verts.Count <= 0) {
            return;
        }

        if (verts.Count <= 0) {
            verts.AddRange(m.verts);
            tris.AddRange(m.tris);
            uvs.AddRange(m.uvs);
            return;
        }

        int count = verts.Count;
        verts.AddRange(m.verts);
        
        for (int i = 0; i < m.tris.Count; i++) {
            tris.Add(m.tris[i] + count);
        }

        uvs.AddRange(m.uvs);
    }

    public Mesh toMesh() {
        Mesh mesh = new Mesh();
        mesh.vertices = verts.ToArray();
        mesh.triangles = tris.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        return mesh;
    }
}
