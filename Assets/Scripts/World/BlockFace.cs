﻿public enum BlockFace {
    NONE,
    UP,
    DOWN,
    NORTH,
    SOUTH,
    EAST,
    WEST
}