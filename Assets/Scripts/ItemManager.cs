﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class ItemManager : MonoBehaviour {

    private const string ITEM_DATA = @"Assets\Resources\conf\items.json";

    public static List<ItemType> ALL;
    public static HashMap<string, ItemType> BY_KEY = new HashMap<string, ItemType>();

    public const string DEFAULT_ICON = "Placeholder";
    public const int DEFAULT_STACK_SIZE = 1;

    // Use this for initialization
    void Awake() {
        // Load Crafting Recipes
        if (File.Exists(ITEM_DATA)) {
            ALL = JsonConvert.DeserializeObject<List<ItemType>>(File.ReadAllText(ITEM_DATA));
            Debug.Log(JsonConvert.SerializeObject(ALL));
        }

        foreach (ItemType type in ALL) {
            if (type.iconPath == null) {
                type.iconPath = DEFAULT_ICON;
            }
            type.icon = Resources.Load(type.iconPath) as Texture;

            if (type.maxStackSize <= 0) {
                type.maxStackSize = DEFAULT_STACK_SIZE;
            }

            BY_KEY[type.key] = type;
        }
    }
}
