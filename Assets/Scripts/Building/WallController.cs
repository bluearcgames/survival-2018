﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {
    
    private bool placed;

    public Transform wallObj;
    public Transform cubeObj;

    public Material woodMat;
    public Material stoneMat;

    public float hp = 100;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!placed) {
            if (Input.GetKeyDown(KeyCode.R)) {
                transform.Rotate(0, 90, 0);
            }

            if (Input.GetMouseButton(0)) {
                placed = true;
                Instantiate(wallObj, transform.position, transform.rotation);
            } else if (Input.GetMouseButton(1)) {
                Destroy(gameObject);
            }

            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                cubeObj.GetComponent<Renderer>().material = woodMat;
                hp = 100;
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                cubeObj.GetComponent<Renderer>().material = stoneMat;
                hp = 200;
            }
        }
	}
}
