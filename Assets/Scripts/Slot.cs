﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {

    public int position;

    private bool hovered;

    public Texture itemIcon;
    private int size;
    
    public InventoryUI inventoryUI;
    private ItemInHandController iihController;

    public Text text;

    // Use this for initialization
    public Slot() {
        hovered = false;
    }

    public void Start() {
        inventoryUI = GameObject.FindWithTag("Player").GetComponent<InventoryUI>();
        iihController = GameObject.FindWithTag("Tools Holder").GetComponent<ItemInHandController>();
    }
	
	// Update is called once per frame
	public void Update() {
        if (inventoryUI != null && inventoryUI.inventory != null && inventoryUI.inventory.items != null && inventoryUI.inventory.items[position] != null) {

            itemIcon = inventoryUI.inventory.items[position].type.icon;
            this.GetComponent<RawImage>().texture = itemIcon;
            this.GetComponent<RawImage>().enabled = true;

            size = inventoryUI.inventory.items[position].size;
            text.text = (size > 1 ? "x" + size : "");
        } else {
            itemIcon = null;
            size = 0;
            this.GetComponent<RawImage>().enabled = false;
        }
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
        hovered = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        hovered = false;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (inventoryUI != null && inventoryUI.inventory != null && inventoryUI.inventory.items != null && inventoryUI.inventory.items[position] != null) {
            ItemStack stack = inventoryUI.inventory.items[position];
            if (stack.type.canUseInHand) {
                iihController.SetActive(stack);
            }
        }
    }
}
