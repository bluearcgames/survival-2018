﻿using System;

[Serializable]
public class ChunkData {

    public BlockData[,,] blocks;

    public int x;
    public int y;
    public int z;
}
