﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class BlockManager : MonoBehaviour {

    private const string BLOCK_TYPE_DATA = @"Assets\Resources\conf\block_types.json";

    public static List<BlockType> ALL;
    public static HashMap<string, BlockType> BY_KEY = new HashMap<string, BlockType>();

    // Use this for initialization
    void Awake() {
        // Load Block Types
        if (File.Exists(BLOCK_TYPE_DATA)) {
            ALL = JsonConvert.DeserializeObject<List<BlockType>>(File.ReadAllText(BLOCK_TYPE_DATA));
            Debug.Log(JsonConvert.SerializeObject(ALL));
        }

        foreach (BlockType type in ALL) {
            BY_KEY[type.key] = type;
        }
    }
}
