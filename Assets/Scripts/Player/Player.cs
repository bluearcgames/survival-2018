﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class Player : MonoBehaviour {
    // variables
    public float maxHealth, maxThirst, maxHunger;
    public float thirstIncreaseRate, hungerIncreaseRate;
    private float health, thirst, hunger;

    public bool dead;

    public GameObject itemInHand;
    public GameObject prevItemInHand;
    public Animation itemInHandAnimation;

    public GameObject player;
    public GameObject hud;
    
    private InventoryUI inventoryUI;

    // Skills
    private PlayerStats skills;
    private const string PLAYER_DATA = @"Assets\Resources\saves\player.json";
    private const string BASE_ATTRIBUTE_DATA = @"Assets\Resources\conf\base_stats.json";

    // functions
    public void Start()
    {
        health = maxHealth;
        inventoryUI = player.GetComponent<InventoryUI>();
        Load();
    }

    public void Update() {
        // thirst and hunger increase
        if (!dead) {
            thirst += thirstIncreaseRate * Time.deltaTime;
            hunger += hungerIncreaseRate * Time.deltaTime;

            if (thirst >= maxThirst) {
                Die();
            }

            if (hunger >= maxHunger) {
                Die();
            }
        }

        if (itemInHand) {
            if (prevItemInHand == null || itemInHand != prevItemInHand) {
                if (prevItemInHand != null) {
                    Destroy(prevItemInHand.GetComponent<ItemInHand>());
                    prevItemInHand.SetActive(false);
                }

                prevItemInHand = itemInHand;

                itemInHand.AddComponent<ItemInHand>();
                itemInHand.GetComponent<ItemInHand>().inventoryUI = inventoryUI;
                itemInHand.SetActive(true);
                itemInHandAnimation = itemInHand.GetComponent<Animation>();
            }

            if (!inventoryUI.inventoryEnabled && Input.GetMouseButton(0) && itemInHandAnimation) {
                if (!itemInHandAnimation.isPlaying) {
                    itemInHandAnimation.Play("pickaxe_swing");
                }
            }
        }
    }

    public void Load() {
        if (File.Exists(PLAYER_DATA)) {
            skills = JsonConvert.DeserializeObject<PlayerStats>(File.ReadAllText(PLAYER_DATA));
        } else {
            skills = JsonConvert.DeserializeObject<PlayerStats>(File.ReadAllText(BASE_ATTRIBUTE_DATA));
            Debug.Log(JsonConvert.SerializeObject(skills));
        }
    }

    public void Die()
    {
        dead = true;
        print("Player has died");
    }
}
