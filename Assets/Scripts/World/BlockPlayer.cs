﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPlayer : MonoBehaviour {

    private BlockType AIR;

    private HashMap<KeyCode, BlockType> creativeBlocks = new HashMap<KeyCode, BlockType>();
    private BlockType activeBlock;
    
	// Use this for initialization
	void Start () {
        AIR = BlockManager.BY_KEY.get("AIR");
        
        creativeBlocks.put(KeyCode.Alpha1, BlockManager.BY_KEY.get("STONE"));
        creativeBlocks.put(KeyCode.Alpha2, BlockManager.BY_KEY.get("BRICK"));
        creativeBlocks.put(KeyCode.Alpha3, BlockManager.BY_KEY.get("WOOD"));
        creativeBlocks.put(KeyCode.Alpha4, BlockManager.BY_KEY.get("GRASS"));
        creativeBlocks.put(KeyCode.Alpha5, BlockManager.BY_KEY.get("IRON"));
        creativeBlocks.put(KeyCode.Alpha6, BlockManager.BY_KEY.get("STONE_STAIRS"));

        activeBlock = creativeBlocks.get(KeyCode.Alpha1);
    }

    // Update is called once per frame
    void Update() {
        RaycastHit hit = new RaycastHit();

        Physics.Raycast(transform.position, transform.forward, out hit, 5f);

        Debug.DrawRay(transform.position, transform.forward, Color.red);

        if (hit.collider != null) {
            if (Input.GetMouseButtonDown(0)) {
                Vector3 point = hit.point + transform.forward * 0.1f;
                Chunk chunk = hit.collider.gameObject.GetComponent<ChunkScript>().chunk;

                Block block = chunk.getBlock(point);
                if (block != null && block.getType() != null) {
                    BlockFace face = GetHitFace(hit);
                    Debug.Log("Clicked " + block.getType().name + " block");
                    Debug.Log("Face: " + face);
                    block.getChunk().setBlock(AIR, block.getLocation());
                }
            } else if (activeBlock != null && Input.GetMouseButtonDown(1)) {
                Vector3 point = hit.point + transform.forward * 0.1f;
                Chunk chunk = hit.collider.gameObject.GetComponent<ChunkScript>().chunk;
                Debug.Log(hit.point);

                Block block = chunk.getBlock(point);
                if (block != null && block.getType() != null) {
                    BlockFace face = GetHitFace(hit);
                    Debug.Log("Face: " + face);
                    Block nBlock = block.getNeighborByFace(face);
                    if (nBlock != null && nBlock.getType() != null && nBlock.getType() == AIR) {
                        Debug.Log("Added " + activeBlock.name + " block");
                        nBlock.getChunk().setBlock(activeBlock, nBlock.getLocation());
                    }
                }
            }

            foreach (KeyCode key in creativeBlocks.keys()) {
                if (Input.GetKeyDown(key)) {
                    activeBlock = creativeBlocks.get(key);
                }
            }
        }
    }

    public BlockFace GetHitFace(RaycastHit hit) {
        Vector3 incomingVec = hit.normal - Vector3.up;
        Debug.Log(incomingVec);

        int x = (int)incomingVec.x;
        int y = (int)incomingVec.y;
        int z = (int)incomingVec.z;

        if (x == 0 && y == -1 && z == -1) {
            return BlockFace.SOUTH;
        }

        if (x == 0 && y == -1 && z == 1) {
            return BlockFace.NORTH;
        }

        if (x == 0 && y == 0 && z == 0) {
            return BlockFace.UP;
        }

        if (x == 1 && y == 1 && z == 1) {
            return BlockFace.DOWN;
        }

        if (x == -1 && y == -1 && z == 0) {
            return BlockFace.WEST;
        }

        if (x == 1 && y == -1 && z == 0) {
            return BlockFace.EAST;
        }

        return BlockFace.NONE;
    }
}
