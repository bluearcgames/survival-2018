﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootTableComponent {

    public int min = 1;
    public int max = 1;
    public float probability = 1.0f;
    
    public string typeKey;
    
    public ItemType getType() {
        return ItemManager.BY_KEY.get(typeKey);
    }
}
