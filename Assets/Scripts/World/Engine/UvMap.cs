﻿using System.Collections.Generic;
using UnityEngine;

public class UvMap {
    private static HashMap<string, UvMap> maps = new HashMap<string, UvMap>();

    public readonly string name;
    public readonly List<Vector2> uvMap;
    
    public readonly float startX;
    public readonly float startY;
    public readonly float textXSize;
    public readonly float textYSize;

    public UvMap(string name, List<Vector2> uvMap, float startX, float startY, float textXSize, float textYSize) {
        this.name = name;
        this.uvMap = uvMap;
        this.startX = startX;
        this.startY = startY;
        this.textXSize = textXSize;
        this.textYSize = textYSize;
    }

    public void register() {
        UnityEngine.Debug.Log(name);
        foreach (Vector2 v2 in uvMap) {
            UnityEngine.Debug.Log(v2);
        }
        maps.put(name, this);
    }

    public static UvMap getUvMap(string name) {
        if (!maps.ContainsKey(name)) {
            return null;
        }

        return maps.get(name);
    }
}