﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class InventoryUI : MonoBehaviour {

    public const int INVENTORY_SLOT_SIZE = 60;
    public const int INVENTORY_SLOT_SPACE = 6;
    public const int INVENTORY_DEFAULT_MAX_WIDTH = 8;

    public GameObject tools;
    private ItemInHandController iihController;
    private Player player;

    public GameObject hud;
    public GameObject inventoryScreen;
    public GameObject slotHolder;
    public GameObject slotPrefab;
    public bool inventoryEnabled;

    public int size;
    private Transform[] slotsTransforms;

    private GameObject itemPickedUp;
    private RigidbodyFirstPersonController fpc;

    public Inventory inventory;

    // Use this for initialization
    public void Start() {
        inventory = new Inventory(size);

        player = GameObject.Find("Player").GetComponent<Player>();
        iihController = tools.GetComponent<ItemInHandController>();

        inventoryScreen.SetActive(inventoryEnabled);
        fpc = GameObject.Find("Player").GetComponent<RigidbodyFirstPersonController>();
        
        // Build UI
        slotsTransforms = new Transform[size];
        int rows = size / INVENTORY_DEFAULT_MAX_WIDTH;
        int numInLastRow = size - ((rows - 1) * INVENTORY_DEFAULT_MAX_WIDTH);

        Vector3 parentPos = slotHolder.transform.position;

        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < INVENTORY_DEFAULT_MAX_WIDTH && (y + 1 < rows || x < numInLastRow); x++) {
                GameObject slot = Instantiate(slotPrefab);
                slot.transform.GetChild(1).GetComponent<Slot>().position = x + y * INVENTORY_DEFAULT_MAX_WIDTH;
                slot.transform.GetChild(1).GetComponent<Slot>().inventoryUI = this;
                slot.transform.SetParent(slotHolder.transform, false);
                slot.transform.localPosition = new Vector3(x * (INVENTORY_SLOT_SPACE + INVENTORY_SLOT_SIZE), -y * (INVENTORY_SLOT_SPACE + INVENTORY_SLOT_SIZE), 0);
                slotsTransforms[x + y * INVENTORY_DEFAULT_MAX_WIDTH] = slot.transform;
            }
        }
    }

    // Update is called once per frame
    public void Update() {
        if (Input.GetKeyDown(KeyCode.I)) {
            if (inventoryEnabled) {
                CloseInventory();
            } else {
                OpenInventory();
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (inventoryEnabled) {
                CloseInventory();
            }
        }
    }

    public void OpenInventory() {
        inventoryEnabled = true;
        inventoryScreen.SetActive(true);
        fpc.mouseLook.lockCursor = false;
        Cursor.lockState = CursorLockMode.None;
        fpc.enabled = false;
        Cursor.visible = true;
    }

    public void CloseInventory() {
        inventoryEnabled = false;
        inventoryScreen.SetActive(false);
        fpc.mouseLook.lockCursor = true;
        Cursor.lockState = CursorLockMode.Locked;
        fpc.enabled = true;
        Cursor.visible = false;
    }

    public void ClickItem(ItemStack item) {
        iihController.SetActive(item);
    }

    public void ItemAdded(ItemStack newItem) {
        this.hud.transform.GetChild(0).transform.GetChild(1).GetComponent<Text>().text = "+" + newItem.size;
        this.hud.transform.GetChild(0).transform.GetChild(2).GetComponent<RawImage>().texture = newItem.type.icon;
    }
}
