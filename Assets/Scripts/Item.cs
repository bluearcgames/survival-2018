﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {

    public ItemType type;
    public string typeKey;

    // Use this for initialization
    void Start () {
        type = ItemManager.BY_KEY.get(typeKey);
    }
}
