﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CraftingWindow : MonoBehaviour {

    public string category;
    public GameObject recipePrefab;

	// Use this for initialization
	void Start () {
		foreach (CraftingRecipe recipe in CraftingManager.BY_CATEGORY.get(category)) {
            Vector3 parentPos = transform.position;
            
            GameObject slot = Instantiate(recipePrefab);
            slot.GetComponent<CraftingSlot>().recipe = recipe;
            slot.transform.SetParent(transform, false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
