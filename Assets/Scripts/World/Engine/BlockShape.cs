﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BlockShape {

    public readonly List<Vector3> verts;
    public readonly List<int> tris;
    public readonly List<Vector2> uvs;

    public BlockShape(List<Vector3> verts, List<int> tris, List<Vector2> uvs) {
        this.verts = verts;
        this.tris = tris;
        this.uvs = uvs;
    }
}
