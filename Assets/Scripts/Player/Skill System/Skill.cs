﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Skill {
    public int level;

    public int maxLevel;

    public HashMap<string, float> attributeEffectsPerLevel;
}
