﻿using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class World : Loopable {
    public const short WORLD_SIZE_X = 6;
    public const short WORLD_SIZE_Y = 1;
    public const short WORLD_SIZE_Z = 6;

    public const short DEFAULT_CHUNK_SIZE_X = 16;
    public const short DEFAULT_CHUNK_SIZE_Y = 16;
    public const short DEFAULT_CHUNK_SIZE_Z = 16;

    private Chunk[,,] chunks;
    private short chunkSizeX;
    private short chunkSizeY;
    private short chunkSizeZ;

    public static World instance;

    private Thread worldThread;
    private bool isRunning;
    private bool ranOnce = false;
    private List<Chunk> loadedChunks = new List<Chunk>();

    public World() {
        instance = this;
    }

    public void start() {
        // Load World Data

        chunks = new Chunk[WORLD_SIZE_X, WORLD_SIZE_Y, WORLD_SIZE_Z];
        
        chunkSizeX = DEFAULT_CHUNK_SIZE_X;
        chunkSizeY = DEFAULT_CHUNK_SIZE_Y;
        chunkSizeZ = DEFAULT_CHUNK_SIZE_Z;

        isRunning = true;

        worldThread = new Thread(run);
        worldThread.Start();
    }

    public void run() {
        Logger.log("Initializing world thread...");

        while (isRunning) {
            try {
                if (!ranOnce) {
                    for (int x = 0; x < WORLD_SIZE_X; x++) {
                        for (int y = 0; y < WORLD_SIZE_Y; y++) {
                            for (int z = 0; z < WORLD_SIZE_Z; z++) {
                                Chunk c = new Chunk(this, x, y, z);
                                chunks[x, y, z] = c;
                                loadedChunks.Add(c);
                            }
                        }
                    }

                    foreach (Chunk c in loadedChunks) {
                        c.start();
                    }

                    ranOnce = true;
                    Logger.log("World thread successfully initialized.");
                }

                foreach (Chunk c in loadedChunks) {
                    c.update();
                }
            } catch (System.Exception e) {
                Logger.log(e);
            }
        }

        Logger.log("World thread successfully stopped.");
        Logger.mainLog.update();
    }

    public void update() {
        if (ranOnce && loadedChunks.Count > 0) {
            foreach (Chunk c in loadedChunks) {
                c.onUnityUpdate();
            }
        }
    }

    public void onApplicationQuit() {
        isRunning = false;
    }

    public Chunk getChunk(Vector3 loc) {
        return getChunk((int)loc.x, (int)loc.y, (int)loc.y);
    }

    public Chunk getChunk(int x, int y, int z) {
        if (chunks.GetLength(0) <= x || x < 0) {
            return null;
        }

        if (chunks.GetLength(1) <= y || y < 0) {
            return null;
        }

        if (chunks.GetLength(2) <= z || z < 0) {
            return null;
        }

        return chunks[x, y, z];
    }

    public Block getBlock(Vector3 loc) {
        return getBlock((int)loc.x, (int)loc.y, (int)loc.z);
    }

    public Block getBlock(int x, int y, int z) {
        int chunkX = x / chunkSizeX;
        int chunkY = y / chunkSizeY;
        int chunkZ = z / chunkSizeZ;

        Chunk chunk = getChunk(chunkX, chunkY, chunkZ);
        if (chunk == null) {
            return null;
        }

        int blockX = x % chunkSizeX;
        int blockY = y % chunkSizeY;
        int blockZ = z % chunkSizeZ;

        return chunk.getBlock(blockX, blockY, blockZ);
    }

    public void addBlock(Vector3 loc, BlockType type) {
        int chunkX = Mathf.FloorToInt(loc.x / chunkSizeX);
        int chunkY = Mathf.FloorToInt(loc.x / chunkSizeY);
        int chunkZ = Mathf.FloorToInt(loc.z / chunkSizeZ);

        try {
            Chunk chunk = getChunk(chunkX, chunkY, chunkZ);
            if (chunk == null) {
                Logger.log("Attempted to add block outside of world");
                return;
            }

            int x = (int)loc.x % chunkSizeX;
            int y = (int)loc.y % chunkSizeY;
            int z = (int)loc.z % chunkSizeZ;
            chunk.setBlock(type, x, y, z);
        } catch (System.Exception e) {
            Logger.log(e);
        }
    }

    public short getChunkSizeX() {
        return chunkSizeX;
    }

    public short getChunkSizeY() {
        return chunkSizeY;
    }

    public short getChunkSizeZ() {
        return chunkSizeZ;
    }
}