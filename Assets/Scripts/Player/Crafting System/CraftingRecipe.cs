﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CraftingRecipe {

    public List<string> categories;
    public string recipeKey;

    public string typeKey;

    public double craftingTime;
    public string craftingAttribute = null;

    public HashMap<string, int> ingredients;

    public float getCraftingTime(float attribute) {
        return 1;
    }

    public ItemType getType() {
        return ItemManager.BY_KEY.get(typeKey);
    }
}
