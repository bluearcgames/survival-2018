﻿public interface Loopable {
    void start();
    void update();
    void onApplicationQuit();
}