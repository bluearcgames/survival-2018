﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Inventory {
    public const int INVENTORY_DEFAULT_SIZE = 20;

    public int size;
    public ItemStack[] items;

    public HashMap<string, int> itemsByTypeKey  = new HashMap<string, int>();

    public Inventory() {
        size = INVENTORY_DEFAULT_SIZE;
        items = new ItemStack[size];
    }

    public Inventory(int size) {
        this.size = size;
        items = new ItemStack[size];
    }

    public void AddItemStacksFromLootTable(LootTable lootTable) {
        if (lootTable == null) {
            return;
        }

        List<ItemStack> newItems = lootTable.GenerateLoot();

        AddItemStacks(newItems);
    }

    public List<ItemStack> AddItemStacks(ItemStack[] itemStacks) {
        return AddItemStacks(new List<ItemStack>(itemStacks));
    }

    public List<ItemStack> AddItemStacks(List<ItemStack> itemStacks) {
        List<ItemStack> itemsNotAdded = new List<ItemStack>();
        foreach (ItemStack itemStack in itemStacks) {
            ItemStack leftOver = AddItemStack(itemStack);
            if (leftOver != null) {
                itemsNotAdded.Add(leftOver);
            }
        }

        return itemsNotAdded;
    }

    public bool AddItemStack(string typeKey, int size) {
        ItemType type = ItemManager.BY_KEY.get(typeKey);
        if (type == null) {
            return false;
        }

        return AddItemStack(type, size) == null ? true : false;
    }

    public ItemStack AddItemStack(ItemType type, int size) {
        return AddItemStack(new ItemStack(type, size));
    }

    public ItemStack AddItemStack(ItemStack itemStack) {
        bool itemAdded = false;
        int emptySlot = -1;

        for (int i = 0; i < size; i++) {
            if (items[i] != null && items[i].type == itemStack.type) {
                items[i].Add(itemStack);

                if (itemStack.size == 0) {
                    itemAdded = true;
                    break;
                }
            }

            if (items[i] == null && emptySlot == -1) {
                emptySlot = i;
            }
        }

        if (!itemAdded && emptySlot >= 0) {
            items[emptySlot] = itemStack;
            itemAdded = true;
        }

        RecalculateItemsByType();

        if (!itemAdded) {
            return itemStack;
        } else {
            return null;
        }
    }

    public ItemStack SetItemStack(int slotId, ItemStack itemStack) {
        bool itemAdded = false;

        if (items[slotId] == null) {
            items[slotId] = itemStack;
            itemAdded = true;
        }

        if (items[slotId] != null && items[slotId].type == itemStack.type) {
            items[slotId].Add(itemStack);

            if (itemStack.size == 0) {
                itemAdded = true;
            }
        }

        RecalculateItemsByType();

        if (!itemAdded) {
            return itemStack;
        } else {
            return null;
        }
    }

    public ItemStack RemoveBySlot(int slotId) {
        ItemStack stack = items[slotId];
        items[slotId] = null;

        RecalculateItemsByType();

        return stack;
    }

    public List<ItemStack> RemoveByType(ItemType type, int amount) {
        return RemoveByTypeKey(type.key, amount);
    }

    public List<ItemStack> RemoveByTypeKey(string typeKey, int amount) {
        if (!HasAmountByTypeKey(typeKey, amount)) {
            return null;
        }

        int amountRemaining = amount;
        List<ItemStack> removedItems = new List<ItemStack>();
        
        for (int i = 0; i < size; i++) {
            if (items[i] != null && items[i].type.key == typeKey) {
                ItemStack removedItem = items[i].Remove(amountRemaining);
                removedItems.Add(removedItem);

                if (items[i].size == 0) {
                    items[i] = null;
                }

                if (removedItem.size == amountRemaining) {
                    break;
                } else {
                    amountRemaining -= removedItem.size;
                }
            }
        }

        RecalculateItemsByType();

        return removedItems;
    }

    public bool HasAmountByType(ItemType type, int amount) {
        return HasAmountByTypeKey(type.key, amount);
    }

    public bool HasAmountByTypeKey(string typeKey, int amount) {
        if (GetAmountByTypeKey(typeKey) < amount) {
            return false;
        }

        return true;
    }

    public int GetAmountByType(ItemType type) {
        return GetAmountByTypeKey(type.key);
    }

    public int GetAmountByTypeKey(string typeKey) {
        return itemsByTypeKey.get(typeKey);
    }

    private void RecalculateItemsByType() {
        itemsByTypeKey = new HashMap<string, int>();
        for (int i = 0; i < size; i++) {
            if (items[i] != null) {
                if (!itemsByTypeKey.containsKey(items[i].type.key)) {
                    itemsByTypeKey[items[i].type.key] = 0;
                }
                itemsByTypeKey[items[i].type.key] += items[i].size;
            }
        }
    }
}
