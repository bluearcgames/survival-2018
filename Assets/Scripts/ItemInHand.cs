﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInHand : MonoBehaviour {

    private GameObject playerObject;
    public InventoryUI inventoryUI;

    public void Start() {
        playerObject = GameObject.Find("Player");
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Resource" && inventoryUI != null) {
            //GameObject mainBody = other.gameObject.GetComponent<LootTable>().mainBody;
            //Rigidbody rigidbody = mainBody.GetComponent<Rigidbody>();
            //rigidbody.isKinematic = false;
            //rigidbody.AddForce(playerObject.transform.forward);
            inventoryUI.inventory.AddItemStacksFromLootTable(other.gameObject.GetComponent<LootTable>());
        }
    }

    private IEnumerator Destroy(GameObject mainBody) {
        yield return new WaitForSeconds(7);
        print("Destroying");
        foreach (Transform child in mainBody.transform) {
            Destroy(child.gameObject);
        }
        Destroy(mainBody);
    }
}
