﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class LootTableManager : MonoBehaviour {

    private const string LOOT_TABLE_DATA = @"Assets\Resources\conf\loot_tables.json";

    private const int DEFAULT_MIN = 1;
    private const int DEFAULT_MAX = 1;
    private const float DEFAULT_PROBABLITY = 1.0f;

    public static List<LootTableType> ALL;
    public static HashMap<string, LootTableType> BY_KEY = new HashMap<string, LootTableType>();

    // Use this for initialization
    void Awake() {
        // Load Crafting Recipes
        if (File.Exists(LOOT_TABLE_DATA)) {
            ALL = JsonConvert.DeserializeObject<List<LootTableType>>(File.ReadAllText(LOOT_TABLE_DATA));
            Debug.Log(JsonConvert.SerializeObject(ALL));
        }

        foreach (LootTableType type in ALL) {
            BY_KEY[type.key] = type;
        }
    }
}
