﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class BlockTypeMeshData {

    public List<List<float>> verts;
    public List<int> tris;
    public List<List<float>> uvs;

    public BlockTypeMeshData(List<List<float>> verts, List<int> tris, List<List<float>> uvs) {
        this.verts = verts;
        this.tris = tris;
        this.uvs = uvs;
    }

    public BlockShape getShape() {
        List<Vector3> verts = new List<Vector3>();
        foreach(List<float> vert in this.verts) {
            if (vert.Count == 3) {
                verts.Add(new Vector3(vert[0], vert[1], vert[2]));
            }
        }

        List<Vector2> uvs  = new List<Vector2>();
        foreach (List<float> uv in this.uvs) {
            if (uv.Count == 2) {
                uvs.Add(new Vector2(uv[0], uv[1]));
            }
        }

        return new BlockShape(verts, this.tris, uvs);
    }
}
