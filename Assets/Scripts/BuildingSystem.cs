﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingSystem : MonoBehaviour {

    public List<BuildObject> objects = new List<BuildObject>();
    public BuildObject currentObj;
    public Vector3 currentPos;
    public Transform currentPreview;

    public Transform cam;
    public RaycastHit hit;
    public LayerMask layerMask;

    public float offset = 1.0f;
    public float gridSize = 1.0f;

    public bool isBuilding;

	// Use this for initialization
	void Start () {
        currentObj = objects[0];
        ChangeCurrentBuilding();
	}
	
	// Update is called once per frame
	void Update () {
		if (isBuilding) {
            StartPreview();
        }
	}

    public void ChangeCurrentBuilding() {
        GameObject currPreview = Instantiate(currentObj.preview, currentPos, Quaternion.identity);
        currentPreview = currPreview.transform;
    }

    public void StartPreview() {
        if (Physics.Raycast(cam.position, cam.forward, out hit, 10, layerMask)) {
            if (hit.transform != this.transform) {
                ShowPreview(hit);
            }
        }
    }

    public void ShowPreview(RaycastHit currHit) {
        currentPos = currHit.point;
        currentPos -= Vector3.one * offset;
        currentPos /= gridSize;
        currentPos = new Vector3(Mathf.Round(currentPos.x), Mathf.Round(currentPos.y) + currentPreview.localScale.y, Mathf.Round(currentPos.z));
        currentPos *= gridSize;
        currentPos += Vector3.one * offset;
        currentPreview.position = currentPos;
    }
}

[System.Serializable]
public class BuildObject {
    public string name;
    public GameObject preview;
    public int gold;
}