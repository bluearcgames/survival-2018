﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInHandController : MonoBehaviour {

    public GameObject pickaxe;
    
    private Player player;

	// Use this for initialization
	void Start () {
        player = GameObject.Find("Player").GetComponent<Player>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetActive(ItemStack item) {
        switch (item.type.key) {
            case "PICKAXE":
                player.itemInHand = this.pickaxe;
                break;
        }
    }
}
